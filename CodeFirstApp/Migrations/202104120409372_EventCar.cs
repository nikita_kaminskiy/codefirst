namespace CodeFirstApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventCar : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NNEventCars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventId = c.Int(nullable: false),
                        CarId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cars", t => t.CarId, cascadeDelete: true)
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .Index(t => t.EventId)
                .Index(t => t.CarId);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NNEventCars", "EventId", "dbo.Events");
            DropForeignKey("dbo.NNEventCars", "CarId", "dbo.Cars");
            DropIndex("dbo.NNEventCars", new[] { "CarId" });
            DropIndex("dbo.NNEventCars", new[] { "EventId" });
            DropTable("dbo.Events");
            DropTable("dbo.NNEventCars");
        }
    }
}
