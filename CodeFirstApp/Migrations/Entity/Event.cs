﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeFirstApp.Migrations.Entity
{
    public class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<NNEventCar> EventCars { get; set; }

    }
}