﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeFirstApp.Migrations.Entity
{
    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public ICollection<NNEventCar> EventCars { get; set; }


    }
}