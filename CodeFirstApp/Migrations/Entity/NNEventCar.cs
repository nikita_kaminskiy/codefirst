﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeFirstApp.Migrations.Entity
{
    public class NNEventCar
    {
        public int Id { get; set; }

        public virtual Event Event { get; set;}
        public virtual Car Car { get; set; }
        public int EventId { get; set; }
        public int CarId { get; set; }


    }
}